#!/usr/bin/env python3

"""
MODULE:       r.stream.snap_simple
AUTHOR(S):    Stefan Blumentrath
PURPOSE:      Match stream related spatial objects to stream pixels
COPYRIGHT:    (C) 2019 by the GRASS Development Team
              This program is free software under the GNU General Public
              License (>=v2). Read the file COPYING that comes with GRASS
              for details.
"""

#%module
#% description: Match stream related spatial objects to stream pixels
#% keyword: raster
#% keyword: snap
#% keyword: stream
#% keyword: points
#%end

#%option G_OPT_V_INPUT
#% key: objects_vector
#% label: Vector map with objects locations
#% description: Input vector map with locations of stream related spatial objects (e.g. species occurrences, dams, wires...)
#% required: NO
#%end

#%option G_OPT_V_TYPE
#%end

#%option G_OPT_V_FIELD
#%end

#%option G_OPT_DB_WHERE
#%end

#%option G_OPT_R_INPUT
#% key: objects_raster
#% label: Raster map with objects locations
#% description: Input raster map with locations of stream related spatial objects (e.g. species occurrences, dams, wires...)
#% required: NO
#%end

#%option G_OPT_R_INPUT
#% key: direction
#% label: Raster map with flow direction
#% description: Input raster map with flow direction (e.g. from r.stream.extract)
#% required: YES
#%end

#%option
#% key: distance
#% label: Snapping distance (in map units) (default=0)
#% description: Snapping distance (in map units) for snapping occurrences and barriers to closest stream pixel
#% type: integer
#% answer: 0
#%end

#%option
#% key: pixel_type
#% label: Method for representing (default=both)
#% description: Snapping distance (in map units) for snapping occurrences and barriers to closest stream pixel
#% type: string
#% options: inlet,outlet,both,all
#% answer: both
#%end

#%option G_OPT_V_OUTPUT
#% label: Vector points map with isolated clusters of the network
#% description: Snapping distance (in map units) for snapping occurrences and barriers to closest stream pixel
#%end

#%flag
#% key: a
#% label: Treat input slope as absolute values
#%end


#%rules
#% required: where,layer,objects_vector
#% exclusive: objects_vector,objects_raster
#%end

# toDo:
# - write tests
# - implement snapping to closest pixel on stream (in number of pixels or mapunits???)
# - Add vector/raster output
# - Add pixel_type mapcalc

import atexit
import os
from io import BytesIO

import numpy as np
import grass.script as gscript


def cleanup():
    """Remove temporary maps"""
    nuldev = open(os.devnull, 'w')
    gscript.run_command('g.remove', flags='bf', quiet=True,
                        type=['raster'], stderr=nuldev,
                        name=tmp_maps)


def get_outlets(direction):
    """Extract outlet raster points from stream flow direction map"""
    outlet_map = '{}_outlet'.format(tmpname)
    # outlet = (abs({dir})==1&&(isnull({cat}[-1,1])|||{cat}[-1,1]!={cat})))
    mc_expression = """{out}=if((abs({dir})==1&&isnull({dir}[-1,1])) || \
(abs({dir})==2&&isnull({dir}[-1,0])) || \
(abs({dir})==3&&isnull({dir}[-1,-1])) || \
(abs({dir})==4&&isnull({dir}[0,-1])) || \
(abs({dir})==5&&isnull({dir}[1,-1])) || \
(abs({dir})==6&&isnull({dir}[1,0])) || \
(abs({dir})==7&&isnull({dir}[1,1])) || \
(abs({dir})==8&&isnull({dir}[0,1])), \
1,null())""".format(out=outlet_map, dir=direction)
    gscript.run_command('r.mapcalc', expression=mc_expression, quiet=True,
                        overwrite=True)
    tmp_maps.append(outlet_map)
    return outlet_map


def stdout2numpy(stdout=None, sep=',', names=None, missing_values='', usecols=None):
    """Read table like output from grass modules as Numpy array (Python3 only)"""
    if type(stdout) == str:
        stdout = gscript.encode(stdout)
    elif type(stdout) != byte:
        gscript.fatal(_('Unsupported data type'))
    np_array = np.genfromtxt(BytesIO(stdout), dtype=None, delimiter=sep,
                             names=names, missing_values=missing_values,
                             usecols=usecols)
    return np_array


def extract_pixel_types(direction, objects_raster, pixel_type, output):
    """Extract pixels representing raster objects on modeled stream network as given pixel type(s)"""
    outlet_map = '{}_outlet'.format(tmpname)
    """
    Look directions for outlet pixel
    3    2    1
    4    x    8
    5    6    7
    """
    look_direction_outlet = {1: '[-1,1]',
                             2: '[-1,0]',
                             3: '[-1,-1]',
                             4: '[0,-1]',
                             5: '[1,-1]',
                             6: '[1,0]',
                             7: '[1,1]',
                             8: '[0,1]'}
    """
    Look directions for inlet pixel (= invers to outlet)
    7    6    5
    8    x    4
    1    2    3
    """
    look_direction_inlet = {1: '[1,-1]',
                            2: '[1,0]',
                            3: '[1,1]',
                            4: '[0,1]',
                            5: '[-1,1]',
                            6: '[-1,0]',
                            7: '[-1,-1]',
                            8: '[0,-1]'}

    templates = {# Neighboring pixel the stream pixel points to is either NULL or has different cat
                 'outlet': '((abs({dir})=={{key}} && isnull({cat}{{look}})) ||| ({cat}{{look}}!={cat})) ||| \\\n'.format(dir=direction, cat=objects_raster),
                 # No neighboring pixels pointing to stream pixel or the neighboring pixel(s) pointing to stream pixel has different cat
                 'inlet': '(abs({dir}{{look}})=={{key}} && (isnull({cat}{{look}}) ||| {cat}{{look}}!={cat})) ||| \\\n'.format(dir=direction, cat=objects_raster),
                 'both': '(abs({dir})=={{key}} && (isnull({cat}{{look}}) ||| {cat}{{look}}!={cat})) ||| \\\n'.format(dir=direction, cat=objects_raster)}

    mc_template = '{output}=if( \\\n'.format(output=output)
    for key in range(8):
        key += 1
        if pixel_type == 'outlet':
            mc_template += templates[pixel_type].format(key=key, look=look_direction_outlet[key])
        elif pixel_type == 'inlet':
            mc_template += templates[pixel_type].format(key=key, look=look_direction_inlet[key])
        elif pixel_type == 'both':
            mc_template += templates[pixel_type].format(key=key, look=look_direction_outlet[key])
            mc_template += templates[pixel_type].format(key=key, look=look_direction_inlet[key])
        elif pixel_type == 'all':
            output = objects_raster
    mc_template = mc_template.rstrip(' ||| \\\n')
    mc_template += ',{cat},null())'.format(cat=objects_raster)
    gscript.run_command('r.mapcalc', expression=mc_template,
                        overwrite=True, quiet=True)
    return mc_template, output


def get_coords(raster, offset=0, snap=0):
    """Extract coordinates from raster map and assign ID (with possible offset)"""
    # Snapping only needed for:
    # - start_points / start_raster
    # - barrier_points / barrier_raster

    if snap > 0:
        coords = stdout2numpy(gscript.read_command('r.stats', flags='gn',
                                                   quiet=True, input=raster,
                                                   separator=',').rstrip('\n'),
                              names=['x', 'y', 'value'])
        coords[raster] = np.arange(offset + 1, offset + 1 + len(coords['x']), 1)
    else:
        distance = '{}_distance'.format(tmpname)
        distance_rc = '{}_distance_rc'.format(tmpname)
        value = '{}_value'.format(tmpname)
        gscript.run_command('r.grow.distance', input=raster, distance=distance, value=value,
                            metric='squared', overwrite=True, verbose=True)
        gscript.write_command('r.reclass', input=distance, output=distance_rc, rules='-',
                              stdin='0 thru {} = 1\n* = NULL'.format(snap**2),
                              overwrite=True, verbose=True)
        coords = stdout2numpy(gscript.read_command('r.stats', flags='gn', quiet=True,
                                                   input=[value, slope, distance, distance_rc], separator=',').rstrip('\n'),
                              names=['x', 'y'] + [raster, slope, distance, distance_rc])

        # Get sorted "rows"
        coords.sort(order=[raster, distance, slope], axis=0)
        i = np.nonzero(np.diff(coords[raster]))[0] + 1
        coords = coords[np.append(0, i)]
    return coords


"""
options = {
'slope': 'SLOPE_11',
'direction': 'DIR',
'start_points': 'RANDOM',
'where': None,
'layer': '1',
'threshold': '150',
'clusters': 'clusters',
'clusters_reachable': 'clusters_reachable'
}

# Flags are probably not needed (if )
flags = {'a': True,
         'd': True,
         'o': True,
         's': False}
"""


def main():
    """Do the main work"""

    # Define static/global variables
    global tmp_maps
    tmp_maps = []
    global tmpname
    tmpname = gscript.tempname(12)

    # Define user input variables
    # Flags
    a_flag = flags['a']
    d_flag = flags['d']
    o_flag = flags['o']
    s_flag = flags['s']

    # Options
    objects_vector = options['objects_vector']
    layer = options['layer']
    where = options['where']
    vtype = options['type']
    objects_raster = options['objects_raster']
    direction = options['direction']
    distance = options['distance']
    pixel_type = options['pixel_type']
    output = options['output']

    # Get a list of categories for input map
    if objects_vector:

        no_object_error = 'No vector objects of requested typ(s) <{}> found in map <{}>'.format(vtypes, objects_vector)
        # Check if input vector map has objects of requested type
        in_topo = gscript.parse_command('v.info', flags='t', quiet=True,
                                        map=objects_vector)

        # Get number of objects with category (category of areas is stored with centroid)
        if 'area' in vtypes and not 'centroid' in vtypes:
            cat_types = vtypes.replace('area', 'centroid')
        elif 'area' in vtypes and 'centroid' in vtypes:
            cat_types = vtypes.replace('area', '').strip(',').replace(',,', ',')
        else:
            cat_types = vtypes

        objects_n = 0
        for vtype in cat_types.split(','):
            objects_n += int(in_topo['{}s'.format(vtypes)])

        if objects_n == 0:
            gscript.fatal(_(no_object_error))

        # Get list of categories of requested objects
        in_cats = stdout2numpy(gscript.read_command('v.to.db', flags='p', quiet=True,
                                                    map=objects_vector, option='query',
                                                    separator=',', query_layer=layer,
                                                    query_column='cat', where=where,
                                                    type=cat_types).rstrip('\n'),
                               names=['cat'], usecols=1)

        if len(in_cats) == 0:
            gscript.fatal(_(no_object_error))

        # else:
        #     in_cats = stdout2numpy(gscript.read_command('v.category', flags='g', quiet=True,
        #                                                 input=objects_vector, option='print').rstrip('\n'),
        #                            names=['cat'])

        objects_raster = '{}_objects_raster'.format(tmpname)

        gscript.run_command('v.to.rast', input=objects_vector, output=objects_raster,
                            use='cat', layer=layer, type=vtypes, where=where, flags='d' if vtypes == 'line' else None,
                            overwrite=True, verbose=True)

    in_rast_cats = stdout2numpy(gscript.read_command('r.category', quiet=True,
                                                     map=objects_raster,
                                                     separator=',').rstrip('\n'),
                                names=['cat', 'label'])

    if objects_vector:
        # Get a list of categories not rasterized
        missing_cats = np.setdiff1d(in_cats['cat'], in_rast_cats['cat'])
        if len(missing_cats) > 0:
            gscript.verbose(_('Not all objects were rasterized, please check your region settings.'))

    # Get a list of categories already on the stream
    cats_stream = stdout2numpy(gscript.read_command('r.stats', quiet=True, flags='n',
                                                    input=[direction, objects_raster],
                                                    separator=',').rstrip('\n'),
                               names=['direction', 'cat'])

    # Check if all cats are represented on the stream
    disjoint_cats = np.setdiff1d(cats_stream['cat'], in_rast_cats['cat'])

    if len(disjoint_cats) == 0:
        gscript.verbose(_('All objects are represented on the stream, no snapping needed'))


    # get inlet/outlet/both (if not all)
    distance_rc = '{}_distance_rc'.format(tmpname)
    if pixel_type == 'inlet':
        pass
    elif pixel_type == 'outlet':
        pass
    elif pixel_type == 'both':
        pass

    # Snap objects not already on the stream
    # Extract objects not already on the stream
    unsnapped = '{}_unsnapped'.format(tmpname)
    rules = '\n'.join(' = '.join('%i' %x for x in y) for y in np.dstack((disjoint_cats, disjoint_cats))[0])
    gscript.write_command('r.reclass', overwrite=True, verbose=True,
                          input=objects_raster, output=unsnapped, rules='-',
                          stdin='0 thru {} = 1\n* = NULL'.format(snap**2))

    distance = '{}_distance'.format(tmpname)
    distance_rc = '{}_distance_rc'.format(tmpname)
    value = '{}_value'.format(tmpname)

    gscript.run_command('r.grow.distance', input=unsnapped, distance=distance, value=value,
                        metric='squared', overwrite=True, verbose=True)
    gscript.write_command('r.reclass', input=distance, output=distance_rc, rules='-',
                          stdin='0 thru {} = 1\n* = NULL'.format(snap**2),
                          overwrite=True, verbose=True)
    coords = stdout2numpy(gscript.read_command('r.stats', flags='gn', quiet=True,
                                               input=[value, slope, distance, distance_rc], separator=',').rstrip('\n'),
                          names=['x', 'y'] + [raster, slope, distance, distance_rc])

    # Get sorted "rows"
    coords.sort(order=[raster, distance, slope], axis=0)
    i = np.nonzero(np.diff(coords[raster]))[0] + 1
    coords = coords[np.append(0, i)]

    # Merge coordinates for snapped and unsnapped pixels

    gscript.run_command('v.in.ascii', overwrite=True, verbose=True,
                        input=unsnapped, output=output)

    # Write raster history
    gscript.vector_history(output)

if __name__ == "__main__":
    options, flags = gscript.parser()
    atexit.register(cleanup)
    main()
