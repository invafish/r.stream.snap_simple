# r.stream.snap_simple
A GRASS GIS module that snaps vector points to stream raster maps

# Manual
https://invafish.gitlab.io/r.stream.snap_simple

# Author
Stefan Blumentrath, [Norwegian Institute for Nature Research (NINA)](https://www.nina.no/), Oslo, Norway

Written for the [INVAFISH](https://prosjektbanken.forskningsradet.no/#/project/NFR/243910)
project (RCN MILJ&Oslash;FORSK grant 243910)
